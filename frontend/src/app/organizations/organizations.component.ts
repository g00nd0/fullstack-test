import { Component, OnInit } from '@angular/core';
import { OrganizationsService } from './organizations.service';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.css'],
})
export class OrganizationsComponent implements OnInit {
  public organizations;

  constructor(private _organizationsService: OrganizationsService) {}

  ngOnInit() {
    this._organizationsService
      .getOrg()
      .subscribe((data) => (this.organizations = data));
  }
}
