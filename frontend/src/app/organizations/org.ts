export interface Org {
  name: string;
  address: string;
  owner: string;
}
