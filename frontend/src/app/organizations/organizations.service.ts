import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import httpClient from '../axios/http-client';
import { Org } from './org';
import { Observable } from 'rxjs';

@Injectable()
export class OrganizationsService {
  constructor(private http: HttpClient) {}

  getOrg() {
    // const resp = await fetch('http://localhost:4000/api/organization/');
    // const data = await resp.json();
    // const {
    //   results: [org],
    // } = data;
    // return org;

    return this.http.get<Org[]>('http://localhost:4000/api/organization/');
  }
}
