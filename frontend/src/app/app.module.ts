import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomeComponent } from './home/home.component';
import { AccountComponent } from './account/account.component';
import { OrganizationsService } from './organizations/organizations.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    routingComponents,
    AccountComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [OrganizationsService],
  bootstrap: [AppComponent],
})
export class AppModule {}
