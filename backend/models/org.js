const mongoose = require("mongoose");
const departmentsSchema = require("./departments");

const orgSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    departments: [departmentsSchema],
    owner: { type: String, required: true },
    address: { type: String, required: true },
    city: { type: String, required: true },
    state: { type: String, required: true },
    country: { type: String, required: true },
    status: { type: String, default: "Active", enum: ["Active", "Inactive"] },
  },
  { timestamps: true }
);

const Org = mongoose.model("Org", orgSchema);

module.exports = Org;
