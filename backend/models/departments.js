const mongoose = require("mongoose");

const departmentsSchema = new mongoose.Schema(
  {
    name: { type: String, required: false },
    employees: [{ type: String, required: false }],
  },
  { timestamps: true }
);

module.exports = departmentsSchema;
