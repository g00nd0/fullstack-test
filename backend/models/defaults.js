const express = require("express");
const router = express.Router();
const orgSeed = [
  {
    name: "Evil Corp",
    address: "666 satan road",
    owner: "Evil CEO",
    city: "New York City",
    state: "New York",
    country: "USA",
    departments: [
      {
        name: "Management",
        employees: ["Terry Colby", "Tyrell Wellick"],
      },
      {
        name: "Finance",
        employees: ["Finance Guy A", "Finance Guy B"],
      },
      {
        name: "IT",
        employees: ["IT Guy A", "IT Guy B"],
      },
    ],
    status: "Active",
  },
  {
    name: "Acme",
    address: "123 middlenowhere road",
    owner: "Acme Boss",
    city: "New Mexico",
    state: "New Mexico",
    country: "USA",
    departments: [
      {
        name: "Management",
        employees: ["Warner", "Bros"],
      },
      {
        name: "Finance",
        employees: ["Finance Guy C"],
      },
      {
        name: "Engineering",
        employees: ["Wild E. Coyote", "Roadrunner"],
      },
    ],
    status: "Active",
  },
];

const userSeed = [
  {
    firstName: "Allo",
    lastName: "Allo",
    email: "some@email.com",
    password: "aaaa1111",
    status: "Active",
  },
  {
    firstName: "Callo",
    lastName: "Am",
    email: "callo-am@email.com",
    password: "aaaa1111",
    status: "Active",
  },
  {
    firstName: "A",
    lastName: "Name",
    email: "a.name@email.com",
    password: "aaaa1111",
    status: "Active",
  },
];

const modelDefaults = {
  orgSeed: orgSeed,
  userSeed: userSeed,
};

module.exports = modelDefaults;
