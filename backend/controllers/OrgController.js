const express = require("express");
const router = express.Router();
const Org = require("../models/org");
const Departments = require("../models/departments");
const { body, validationResult } = require("express-validator");
const methodOverride = require("method-override");
const modelDefaults = require("../models/defaults");
router.use(methodOverride("_method"));

router.get("/seed", (req, res) => {
  Org.create(modelDefaults.orgSeed, (error, org) => {
    if (error) {
      res.send(error);
    } else {
      console.log(modelDefaults);
      console.log(org);
      res.send(org);
    }
  });
});

router.get("/", (req, res) => {
  //index, get all
  Org.find({}, (error, org) => {
    res.send(org);
  });
  console.log("get all organizations");
});

router.get("/:id", (req, res) => {
  //getting one org
  Org.find({ _id: req.params.id, status: "Active" }, (error, org) => {
    res.send(org);
    return org;
  });
  console.log("get one organisation");
});

router.post(
  // create org
  "/",
  body("name", "Enter Organization Name").notEmpty(),
  body("address", "Please enter an address").notEmpty(),
  body("owner", "Please enter the owner's name").notEmpty(),
  body("city", "Please enter city name").notEmpty(),
  body("state", "Please enter a state.").notEmpty(),
  body("country", "Please enter a Country.").notEmpty(),

  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      // Errors returned in an array `errors.array()`.
      const locals = { UserInput: req.body, errors: errors.array() };
      res.status(StatusCodes.BAD_REQUEST).send(locals);
    } else {
      Org.create(req.body, (error, organization) => {
        if (error) {
          res.send(error);
        } else {
          res.send(organization);
          return organization;
        }
      });
    }
  }
);

router.put("/:id/sdelete", (req, res) => {
  Org.findById(req.params.id, (err, organization) => {
    if (err) {
      res.send(err);
      console.log("error occurred " + err);
    } else {
      organization.status = "Inactive";
      organization.save((er) => {
        if (er) {
          res.send(er);
        } else {
          res.send(organization);
        }
      });
      console.log("soft delete");
    }
  });
});

router.put("/:id/edit", (req, res) => {
  const newOrg = req.body;

  Org.findById(req.params.id, (err, organization) => {
    if (err) {
      res.send(err);
      console.log("error occurred " + err);
    } else {
      organization.name = newOrg.name;
      organization.owner = newOrg.owner;
      organization.address = newOrg.address;
      organization.city = newOrg.city;
      organization.state = newOrg.state;
      organization.country = newOrg.country;
      organization.save((er) => {
        if (er) {
          res.send(er);
        } else {
          res.send(organization);
        }
      });
      console.log("edit organization");
    }
  });
});

module.exports = router;
