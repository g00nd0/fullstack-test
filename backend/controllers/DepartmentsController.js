const express = require("express");
const router = express.Router();
const Org = require("../models/org");
const Departments = require("../models/departments");
const methodOverride = require("method-override");
const modelDefaults = require("../models/defaults");
router.use(methodOverride("_method"));

router.get("/", (req, res) => {
  //index, get all
  Org.find({}, (error, org) => {
    res.send(org);
  });
  console.log("get all departments");
});

module.exports = router;
